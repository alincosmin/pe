﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace IP_L1_P2
{
    class Program
    {
        static void Main()
        {
            var employees = new List<IEmployee>();

            var dev = new Developer
            {
                Name = "Andrei",
                Language = Developer.ProgrammingLanguage.Python
            };

            employees.Add(dev);
            
            var tester = new Tester
            {
                Name = "Bogdan",
                Type = Tester.TypeOfTesting.Automated
            };

            employees.Add(tester);
            
            var tl = new TeamLeader
            {
                Name = "Team Leader",
                SubOrdinates = new List<ISimpleEmployee>()
            };
            
            employees.Add(tl);

            tl.SubOrdinates.Add(dev);
            tl.SubOrdinates.Add(tester);
           
            var gl = new GroupLeader
            {
                Name = "Group Leader",
                SubOrdinates = new List<TeamLeader>()
            };
            gl.SubOrdinates.Add(tl);
            
            employees.Add(gl);

            var manager = new Manager();
            manager.SubOrdinates = employees;
            var json = JsonConvert.SerializeObject(manager);
            System.IO.File.WriteAllText("abc.txt",json);
            //var json2 = System.IO.File.ReadAllText("xyz.txt");
            //Console.WriteLine("{0} {1}",json.Length,json2.Length);
            //var manager2 = (Manager) JsonConvert.DeserializeObject(json2, typeof (Manager));
            //Console.WriteLine(manager2.SubOrdinates.First().Name);

        }
    }
}
