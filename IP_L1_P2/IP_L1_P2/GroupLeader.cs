﻿using System.Collections.Generic;

namespace IP_L1_P2
{
    public class GroupLeader : IEmployee
    {
        public string Name { get; set; }
        public List<TeamLeader> SubOrdinates { get; set; } 
    }
}