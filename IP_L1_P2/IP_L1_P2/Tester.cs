﻿namespace IP_L1_P2
{
    public class Tester : ISimpleEmployee
    {
        public enum TypeOfTesting
        {
            Manual,
            Automated
        };
        public TypeOfTesting Type { get; set; }
        public string Name { get; set; }
    }
}