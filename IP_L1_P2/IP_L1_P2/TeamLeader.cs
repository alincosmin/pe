﻿using System.Collections.Generic;

namespace IP_L1_P2
{
    public class TeamLeader : IEmployee
    {
        public string Name { get; set; }
        public List<ISimpleEmployee> SubOrdinates { get; set; }
    } 
}