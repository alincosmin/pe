﻿namespace IP_L1_P2
{
    public class Developer : ISimpleEmployee
    {
        public enum ProgrammingLanguage
        {
            C,
            Java,
            Python
        }
        public ProgrammingLanguage Language { get; set; }
        public string Name { get; set; }
    }
}